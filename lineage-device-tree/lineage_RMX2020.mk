#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from RMX2020 device
$(call inherit-product, device/realme/RMX2020/device.mk)

PRODUCT_DEVICE := RMX2020
PRODUCT_NAME := lineage_RMX2020
PRODUCT_BRAND := realme
PRODUCT_MODEL := RMX2020
PRODUCT_MANUFACTURER := realme

PRODUCT_GMS_CLIENTID_BASE := android-oppo

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="aospa_RMX2020-userdebug 13 TKQ1.230420.001 eng.sartha.20230512.174227 test-keys"

BUILD_FINGERPRINT := realme/aospa_RMX2020/RMX2020:13/TKQ1.230420.001/sarthakroy200205121742:userdebug/test-keys
